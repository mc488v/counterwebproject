package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class CountNumTest {
    @Test
    public void testMax() throws Exception {

        int k= new  CountNum().findMax(10,5);
        assertEquals("max", 2, k);

    }
   @Test
    public void testMax2() throws Exception {

        int k= new CountNum().findMax(10,0);
        assertEquals("max", Integer.MAX_VALUE, k);

    }

}

